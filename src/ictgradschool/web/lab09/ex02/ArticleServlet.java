package ictgradschool.web.lab09.ex02;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ArticleServlet extends HttpServlet {
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // request.getParameter accesses parameters from the submitted form
        String atitle = request.getParameter("atitle");
        String aauthor = request.getParameter("aauthor");
        String section = request.getParameter("section");
        String content = request.getParameter("content");

        // printWriter is used to write to the HTML document
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<h2>" + atitle + "</h2>");
        out.println("<p>By: " + aauthor + "<br>");
        out.println("Genre: " + section + "</p>");
        out.println("<p>" + content + "</p>");
        out.println("<h3>This is a string array from Java:</h3>");

        String[] arrayForWebsite = new String[]{"Four", "Three", "Two", "One"};
        out.println("<ul>");
        for (int i = 0; i<arrayForWebsite.length; i++){
            out.println("<li>" + arrayForWebsite[i] + "</li>");
        }
        out.println("</ul>");
    }
}
